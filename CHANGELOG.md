# Changelog

## [Unreleased]

## [2.1.1] - 2024-07-09

### Fixed

- Trying to fix gitlab release asset

## [2.1.0] - 2024-07-09

### Added

- Added option to avoid flying when doing deliveries. Turned on by default, as the npcs are usually close enough to each other that mounting will look weird.
- Added Dawntrail Leve Data

### Changed

- Leve data js script revised

### Removed

- Removed superfluous logging


## [2.0.0] - 2022-01-14

### Added
- Added class filters. Just click the filter to show only leves for that class. Class filters are exclusive.
- Added favorites list and filter. Right click a Leve to add it to the favorites list. The favorites filter can be combined with class filters.
- Added info panel. Click on a Leve to see additional info.
- Added all missing levequests to the database. FSH leves included.
- Added tooltips to various sections of the GUI.
- Added tools to help fill the gaps on the database. Are commented out by default.
- Added a "Force reset" checkbox+button to help clear all settings on the Minion DB.

### Changed
- Levequest list are now sorted by class and then ID.
- Changed DB structure to store more info and make it easier to get the leves.
- Changed most DB access and leve listing to work exclusively by contentID. Should work regardless of language in most cases.
- Made the LeveListBuilder script much more robust. Now we can get almost all info on a levequest, missing just the levemete name/id.

### Fixed
- Fix delivery logic for real this time. Limited the points where the item count is updated and separated from the quest status update.

## [1.3.2] - 2021-12-15

### Fixed
- Fix typo on UpdateLeveCounters calll

## [1.3.1] - 2021-12-15

### Fixed
- Fix currentLeve ending up with invalid values

## [1.3.0] - 2021-12-14

### Fixed
- Fix release script one more time I guess

## [1.2.0] - 2021-12-14

### Fixed
- Fix release script and bump to correct version number

## [1.1.5] - 2021-12-14

### Fixed
- Fix counting errors during deliveries in general.

### Added
- Add search box to levequest list
- Add all Endwalker leves
- Add Node script to help aquire levequests from XIVAPI

## [1.1.4] - 2021-11-06

### Fixed

- Fix counting error on items being delivered during triple levequests.
- Fix old conversation panels not working due to having 5 entries instead of 4.
- Fix some entries on the DB.lua that had swapped y and z values on npc position data. There are probably more npcs like this, will try to go through them all later on and check.

## [1.1.3] - 2021-07-03

### Added

- Add comments and doclet annotations to more of the code
- Add data provided by @Warblr#1312 on discord for many levequests and npcs

## [1.1.2] - 2021-03-10

### Added

- Add comments and doclet annotations to multiple sections of the code

### Fixed

- Fix typo on "Keeping Loyalty" entry

## [1.1.1] - 2021-03-02

### Fixed

- Fix "Keeping Loyalty" item required

## [1.1.0] - 2021-03-01

### Added

- Add "Keeping Loyalty" Levequest to DB

## [1.0.0] - 2021-01-23

### Added

- New tab for settings
- New delay setting the delivery and acceptance of quests, making the overall usage of the module slower
- Added a new boolean field to the levequest table to indicate weather it is a triple delivery or not, denoted on the list by a "(3)" after the leve name
- New list of items to deliver on the main task window
- Added an option to use the item name instead of the levequest name on the lists
- Lists are saved across sessions
- You can start / stop the LeveDelivery bot mode from within the list window

### Changed

- Reworked and consolidated cause and effects
- UI was reorganized
  - Removed columns widget
  - Better spacing between widgets
  - Pretty tabs! (Special thanks to [Kali](https://github.com/KaliMinion) for inspiration)
- DB was moved to a separate file `DB.lua`
- DB changes
  - `['target']` renamed to `['taker']`
  - `['isTriple]` boolean added

### Fixed

- Fixed delivery counting not working properly

## [0.0.0]

---
