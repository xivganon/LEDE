import { format } from 'lua-json'
import * as fs from 'node:fs/promises'

const boilmaster_url= new URL('https://beta.xivapi.com')

async function getLeveIdList(initial_row) {
  let last_row = initial_row
  let arrayLeveId = []
  console.log("Array Init")
  const leve_url = new URL(`api/1/sheet/Leve`, boilmaster_url)
  leve_url.searchParams.set(`after`, last_row)
  console.log("URL is " + leve_url.toString())
  while(true) {
    console.log("starting loop")
    const response = await fetch(leve_url)
    let json_response
    console.log("Request done")
    if (response) {
      console.log("Response get")
      json_response = await response.json()
    }
    if (json_response) {
      if (json_response.rows.length < 100) {
        arrayLeveId = arrayLeveId.concat(json_response.rows)
        break
      } else {
        arrayLeveId = arrayLeveId.concat(json_response.rows)
        last_row += 100
        leve_url.searchParams.set(`after`, last_row)
        console.log("Next Page")
      }
    }
  }
  console.log("Actually done")
  console.log(arrayLeveId.length + " items")
  return arrayLeveId
}

async function getLeveData(current_row) {
  console.log("getting leve")
  const leve_data_url = new URL(`api/1/sheet/Leve/${current_row}`, boilmaster_url)
  console.log("URL is ready " + leve_data_url)
  leve_data_url.searchParams.set(`fields`,`Name,ClassJobCategory.Name,ClassJobLevel,DataId.Item[].Name,LevelLevemete.Object,DataId.ItemCount[],DataId.Repeats`)
  const response = await fetch(leve_data_url)
  console.log("got response")
  const response_object = await response.json()
  if(response_object.fields.ClassJobCategory.fields.Name == "BTN" || response_object.fields.ClassJobCategory.fields.Name == "MIN") {
    console.log("It's MIN or BTN")
    return null
  }
  const json_entry = {
    id : response_object.row_id,
    item: {
      id : response_object.fields.DataId.fields.Item[0].row_id,
      name : response_object.fields.DataId.fields.Item[0].fields.Name,
      quantity : response_object.fields.DataId.fields.ItemCount[0],
    },
    job : response_object.fields.ClassJobCategory.fields.Name,
    levelRequired : response_object.fields.ClassJobLevel,
    levemeteID : "",
    levemeteName : "",
    name: response_object.fields.Name,
    targetID : response_object.fields.LevelLevemete.fields.Object.row_id,
    targetName : "",
    triple : response_object.fields.DataId.fields.Repeats == 0 ? false : true,
  }
  console.log("done with "+current_row)
  return json_entry
}


let entries = []
entries = await getLeveIdList(1687)

const leve_db = {
  CRP: {},
  BSM: {},
  ARM: {},
  GSM: {},
  LTW: {},
  WVR: {},
  ALC: {},
  CUL: {},
  FSH: {},
}

for (const current_row of entries) {
  console.log("Current row is " + current_row.row_id)
  const leve = await getLeveData(current_row.row_id)
  if (leve) {
    leve_db[leve.job][current_row.row_id] = leve
    const lua_leves = format(leve_db)
    await fs.writeFile("output.lua", lua_leves);
  }
}

console.log("all done")