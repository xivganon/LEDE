# AnonAddons LeveDelivery

## Description

This **[FFXIVMinion](https://github.com/MINIONBOTS/FFXIVMinion)** addon adds a 'Leve Delivery' bot mode to help hand in crafting and fishing levequests.

Remember that this **will NOT craft or gather** the required items. Only hand them in.

As always, if you enjoy my work, [consider donating to help me out](https://ko-fi.com/xivg_anon). Every bit helps :) 

---

## Instalation

To install just follow the same steps described in the [CORE installation](https://gitlab.com/xivganon/CORE#installation). The end result should be like this:

![Folders](https://i.ibb.co/4JRqJKY/image.png "Each module has its own folder")

---

## Usage

Open the Levequest list manager either through the AnonAddons menu entry or the big button on the `Leve Delivery` main task mode.

![MenuEntry](https://i.ibb.co/H2ByLML/image.png "Either this...")

![MainTask](https://i.ibb.co/JqZtZ6D/image.png "...or that")

After that you will meet the list manager

![ListManager](https://i.ibb.co/hfFxDw4/image.png "Here it is")

There's quite a few things to help you with the list: 
 - At the top left, in silver, we have the available class filters. These will remove all leves from the list EXCEPT the ones that match the selected class.
 - To the right of the class filter, we have the favorite filter. You can set a levequest as favorite by right clicking it's name on the list. This will put a star on the entry, and the levequest will be listed when you filter by favorites. The favorite filter can be combined with the class filter.
 - Below the class filters we have the search bar, where you can filter the list by specific words.
 - On the middle-left section is the actual levequest list.
 - On the middle-right section you can see additional information about a selected levequest.
 - On the bottom section you have the start/stop button.

When you add levequests with the "Add to list" button, you end up with this look:

![LevesAdded](https://i.ibb.co/x7Yy6Ny/image.png, "With added levequests")

After adding that you can change how many times should that levequest be run. **Remember that this is the ammount of levequests done, and NOT the ammount of the items delivered.** Ex: A triple quest will use 3 times the item required but will only count as one leve done.

The Main Task window will show you how many items you have, and how many are needed

![ItemCount](https://i.ibb.co/tPTM1p8/image.png "So convenient!")

After that you just need to presst the `Start/Stop` button on the main task window, or the big `Start` button on the levequest list window, and it will deliver all the items you have prepared!

---

## The settings tab

On the settings tab you'll have some small tools available.

![SettingsTab](https://i.ibb.co/pL1rhMK/image.png, "What does this button do?")

 - `Action Delay` value os the time the bot will wait before accepting a levequest, and right after handing over the delivery. This value is set in milliseconds. If you think the delivery goes too fast, feel free to raise this value.
 - `Show item name instead of levequest name` is very descriptive. Some people might prefer to search the levequest list by the item to deliver instead of the levequest name.
 - `Activate debug logging` will add extra strings on the console as the bot runs. Helps figuring out why a problem is ocurring. Please use this before reporting a problem, and send the relevante console log with the report.
 - `Enable force reset` will show a button that will reset all you configurations and pending list. Can be useful when something goes wrong.

---

## Understanding the levequest list! 

Right now the list should be 100% complete, with every levequest delivery listed, including FSH deliveries. This is the main section of the addon that will be updated as the game expands.

I'm unsure on how this list behaves on CN/KR clients, so here is the info in case you need to modify the Database itself:

Right now all the levequest info is saved on the [DB.lua](https://gitlab.com/xivganon/LEDE/-/blob/master/LEDE.lua) file. It uses the following structure:

```lua
local DB = { -- DB table
    leves = { -- Levequest list
        CUL = { -- The levequest job
            [272] = { -- The levequest ID
                id = 272, -- The levequest ID
                item = { -- The item needed to complete the levequest
                    id = 4657, -- The item ID
                    name = 'Braised Pipira', -- The Item Name
                    quantity = 3, -- The quantity required to complete one delivery
                },
                job = 'CUL', -- The levequest job
                levelRequired = 5, -- The level required to see the levequest
                levemeteID = 1004342, -- The contentID of the npc that gives the levequest
                levemeteName = 'Wyrkholsk', -- The levemete (npc with the levequest list)'s name
                name = 'Fishy Revelations', -- The levequest name
                targetID = 1001787, -- The contentID of the npc that receives the levequest items
                targetName = 'Bango Zango', -- The target name
                triple = false, -- Whether the levequest is a triple delivery or not
            },
        },
    },

    levemetes = { -- List of npcs that give out levequests
        [1004342] = { -- The levemete NPC contentID
            contentid = 1004342, -- The levemete NPC contentID
            mapid = 135, -- The mapID where the levemete is
            name = 'Wyrkholsk', -- Levemente NPC name
            pos = { -- Minion coordinates to the levemete location
                x = 499.59558105469,
                y = 79.724922180176,
                z = -74.570861816406,
            },
        },
    },

    targets = { -- List of the npcs that give out levequests
        [1001787] = { -- The target NPC content 
            contentid = 1001787, -- The target NPC content 
            mapid = 129, -- The mapID where the target is
            name = 'Bango Zango', -- Target NPC name
            pos = { -- Minion coordinates to the target location
                x = -62.119567871094,
                y = 18.000333786011,
                z = 9.4147338867188,
            },
        },
    },
}

return DB
```