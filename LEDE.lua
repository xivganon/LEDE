--- Leve Delivery
-- @module LEDE
local LEDE = inheritsFrom(ml_task)

--[[
--
--  This module runs levequest pickups and delivery from a list.
--  After the list is set up and the bot is started it will check
--  if there are enough items to complete the leve (and 3 times
--  for the multidelivery quests, toggle planned in the future).
--  Then it heads to the levequest giver, and will pick the current
--  levequest (batch pickup/delivery planned). It then moves to the
--  levequest target and deliver it
--
--]]

-- -------------------------------------------------------------------------- --
-- ANCHOR                         Internal Info                               --
-- -------------------------------------------------------------------------- --

LEDE._VERSION = '2.1.1'
LEDE._NAME = 'Leve Delivery'
LEDE._DESCRIPTION = 'Levequest delivery by name/class'
LEDE._URL = 'https://gitlab.com/xivganon/LEDE'
LEDE._ID = '20298810'

d('[AnonAddons] LeveDelivery Loaded')

-- -------------------------------------------------------------------------- --
-- ANCHOR                           Module Variables                          --
-- -------------------------------------------------------------------------- --

--- Table with GUI variables and definitions
-- @table GUI
-- @field open Wether the levequest list window is open
-- @field visible Wether the levequest list window is expanded
-- @field LeveSelect Selected entry from list dropdown
-- @field search field
-- @field tabSelected Currently selected tab
LEDE.GUI = {
  open = false,
  visible = false,
  LeveSelect = 1,
  search = '',
  tabs = { 'Leve List', 'Settings' }, -- , 'DB Helper' },
  tabSelected = 1,
  icons = {},
  leveFilter = '',
  favFilter = false,
  favorites = {},
}

--- Table with non-persistent variables
-- @table state
-- @field currentLeve Reference to the current leve entry
LEDE.state = {
  currentLeve = nil,
  leveList = {},
}

--- Table with persistent variables
-- @table settings
-- @field avoidFlying Stick to the ground when running deliveries
-- @field delayRate Delay when doing some actions, in milliseconds, currently not as usefull as expected
-- @field useItemName Use item name instead of leve name when displaying the list
-- @field showDebugPrint Extra info
LEDE.settings = {
  avoidFlying = true,
  delayRate = 1000,
  useItemName = false,
  showDebugPrint = false,
}

LEDE.danger = {
  forceEnable = false,
}

-- -------------------------------------------------------------------------- --
-- ANCHOR                            Initialization                           --
-- -------------------------------------------------------------------------- --

--- Module initialization. Called by the mmominion main thread after the initialization event
-- @string event The event that triggered this functions
-- @number ticks Current time count
function LEDE.ModuleInit(event, ticks)
  d('[AnonAddons] LeveDelivery Initializing')

  LEDE.DB = aa.Require(GetLuaModsPath() .. [[AALeveDelivery/DB.lua]])

  -- Add a new bot mode to the mode list in the main task window
  if not ffxivminion.modes[LEDE._NAME] then
    ffxivminion.AddMode(LEDE._NAME, LEDE)
  end

  -- Add submenu entry on the Anon Addons menu entry
  local menuIcon = GetLuaModsPath() .. [[AALeveDelivery/LEDE.png]]
  ml_gui.ui_mgr:AddSubMember(
    {
      id = 'LD##LD',
      name = 'Leve Delivery',
      texture = menuIcon,
      onClick = LEDE.Open,
     }, 'FFXIVMINION##MENU_HEADER', 'AA##AA'
  )
  
  LEDE.GUI.icons = {
    CRP = GetLuaModsPath() .. [[AALeveDelivery/icons/CRP.png]],
    BSM = GetLuaModsPath() .. [[AALeveDelivery/icons/BSM.png]],
    ARM = GetLuaModsPath() .. [[AALeveDelivery/icons/ARM.png]],
    GSM = GetLuaModsPath() .. [[AALeveDelivery/icons/GSM.png]],
    LTW = GetLuaModsPath() .. [[AALeveDelivery/icons/LTW.png]],
    WVR = GetLuaModsPath() .. [[AALeveDelivery/icons/WVR.png]],
    ALC = GetLuaModsPath() .. [[AALeveDelivery/icons/ALC.png]],
    CUL = GetLuaModsPath() .. [[AALeveDelivery/icons/CUL.png]],
    FSH = GetLuaModsPath() .. [[AALeveDelivery/icons/FSH.png]],
    star = GetLuaModsPath() .. [[AALeveDelivery/icons/star.png]],
  }

  -- Retrieve stored settings or initialize them
  LEDE.settings.avoidFlying = Settings.LEDE.avoidFlying or false
  LEDE.settings.delayRate = Settings.LEDE.throttleRate or 1000
  LEDE.settings.useItemName = Settings.LEDE.useItemName or false
  LEDE.settings.favorites = Settings.LEDE.favorites or {}
  LEDE.state.leveList = Settings.LEDE.leveList or {}

  -- Although currentLeve index gets stored, it can cause issues when it loads with a invalid index, so we always reset it
  LEDE.state.currentLeve = nil

  -- Setup API on the 'aa' global table
  aa['LEDE'] = {
    GetInfo = function()
      return {
        Version = LEDE._VERSION,
        Name = LEDE._NAME,
        Description = LEDE._DESCRIPTION,
        Url = LEDE._URL,
        Id = LEDE._ID,
      }
    end,
  }

  QueueEvent('AnonAddons.InitDone', 'LEDE', nil)
end

--- Bot mode init. Not sure when the main process call this.
function LEDE.Init()
  -- LuaFormatter off
  local skipTalk = ml_element:create(
    "skipTalk",
    LEDE.CnE.skipTalk.c,
    LEDE.CnE.skipTalk.e, 10
  )
  LEDE:add(skipTalk, LEDE.overwatch_elements)

  local skipSelectString = ml_element:create(
    "skipSelectString",
    LEDE.CnE.skipSelectString.c,
    LEDE.CnE.skipSelectString.e, 10
  )
  LEDE:add(skipSelectString, LEDE.overwatch_elements)

  local skipSelectIconString = ml_element:create(
    "skipSelectIconString",
    LEDE.CnE.skipSelectIconString.c,
    LEDE.CnE.skipSelectIconString.e, 10
  )
  LEDE:add(skipSelectIconString, LEDE.overwatch_elements)

  local skipSelectYesno = ml_element:create(
    "skipSelectYesno",
    LEDE.CnE.skipSelectYesno.c,
    LEDE.CnE.skipSelectYesno.e, 10
  )
  LEDE:add(skipSelectYesno, LEDE.overwatch_elements)

  local closeGuildLeveList = ml_element:create(
    "closeGuildLeveList",
    LEDE.CnE.closeGuildLeveList.c,
    LEDE.CnE.closeGuildLeveList.e, 10
  )
  LEDE:add(closeGuildLeveList, LEDE.overwatch_elements)

  local handOverLeveItem = ml_element:create(
    "handOverLeveItem",
    LEDE.CnE.handOverLeveItem.c,
    LEDE.CnE.handOverLeveItem.e, 10
  )
  LEDE:add(handOverLeveItem, LEDE.process_elements)

  local moveToNPCMap = ml_element:create(
    "moveToNPCMap",
    LEDE.CnE.moveToNPCMap.c,
    LEDE.CnE.moveToNPCMap.e, 11
  )
  LEDE:add(moveToNPCMap, LEDE.process_elements)

  local moveToNPCPos = ml_element:create(
    "moveToNPCPos",
    LEDE.CnE.moveToNPCPos.c,
    LEDE.CnE.moveToNPCPos.e, 12
  )
  LEDE:add(moveToNPCPos, LEDE.process_elements)

  local interactWithNPCNearby = ml_element:create(
    "interactWithNPCNearby",
    LEDE.CnE.interactWithNPCNearby.c,
    LEDE.CnE.interactWithNPCNearby.e, 13
  )
  LEDE:add(interactWithNPCNearby, LEDE.process_elements)

  local acceptLeveQuest = ml_element:create(
    "acceptLeveQuest",
    LEDE.CnE.acceptLeveQuest.c,
    LEDE.CnE.acceptLeveQuest.e, 14
  )
  LEDE:add(acceptLeveQuest, LEDE.process_elements)

  local completeLeveQuest = ml_element:create(
    "completeLeveQuest",
    LEDE.CnE.completeLeveQuest.c,
    LEDE.CnE.completeLeveQuest.e, 15
  )
  LEDE:add(completeLeveQuest, LEDE.process_elements)

  local selectNextLeve = ml_element:create(
    "selectNextLeve",
    LEDE.CnE.selectNextLeve.c,
    LEDE.CnE.selectNextLeve.e, 16
  )
  LEDE:add(selectNextLeve, LEDE.overwatch_elements)
-- LuaFormatter on

  for _, leve in pairs(LEDE.state.leveList) do
    LEDE.UpdateLeveCounters(leve)
  end
end

--- Override create method from base task table. Necessary for proper mode functionality
function LEDE.Create()
  LEDE.valid = true
  LEDE.completed = false
  LEDE.subtask = nil
  LEDE.auxiliary = false
  LEDE.process_elements = {}
  LEDE.overwatch_elements = {}
  return LEDE
end

-- -------------------------------------------------------------------------- --
-- ANCHOR                           Rendering                                 --
-- -------------------------------------------------------------------------- --

--- GUI display toggle
-- @function Open
function LEDE.Open()
  LEDE.GUI.open = true
end

--- Levelist builder GUI creation
-- @function ModuleDraw
-- @string event The event that triggered this functions
-- @number ticks Current time count
function LEDE.ModuleDraw(event, ticks)

  -- Flag to indicate wether settings changed and should be saved
  local save = false

  -- Only do the draw function if the window is open
  if (LEDE.GUI.open) then

    -- Center the window position in case it is open for the first time
    GUI:SetNextWindowPosCenter(GUI.SetCond_FirstUseEver)
    -- Keep the window at a fixed width and auto-adjust the height
    GUI:SetNextWindowSize(700, 0, GUI.SetCond_Always)

    -- Start the window definition
    LEDE.GUI.visible, LEDE.GUI.open = GUI:Begin('Leve Delivery##LDMainWindow', LEDE.GUI.open)

    -- Only process if window is not minimized
    if (LEDE.GUI.visible) then
      -- Get the available width
      local fullWidth = GUI:GetContentRegionAvail()

      -- ---------------------------------- Tabs ---------------------------------- --

      aa.PushStyle(aa.Layouts.tabHeaderButton) -- Push the general tab style
      GUI:BeginChild('tabs', 0, 30, true, GUI.WindowFlags_AlwaysAutoResize) -- Create a new region to contain the window tabs
      for index, tabName in pairs(LEDE.GUI.tabs) do -- Loop for each tab entry
        if LEDE.GUI.tabSelected == index then -- If we are drawing the currently selected tab
          aa.PushStyle(aa.Layouts.tabHeaderButtonSelected) -- Push the selected tab style
          if GUI:Button(tabName, fullWidth / table.size(LEDE.GUI.tabs), 30) then -- When the defined button is clicked
            LEDE.GUI.tabSelected = index -- Change the selected tab
          end
          aa.PopStyle(aa.Layouts.tabHeaderButtonSelected) -- Pop the selected tab style so we don't break everything
        else
          if GUI:Button(tabName, fullWidth / table.size(LEDE.GUI.tabs), 30) then -- Same process than before but we don't change from the base style
            LEDE.GUI.tabSelected = index -- change the selected tab
          end
        end
        GUI:SameLine()
      end
      GUI:NewLine()
      GUI:EndChild() -- Close the tab region
      aa.PopStyle(aa.Layouts.tabHeaderButton) -- Pop the general tab style
      GUI:Dummy(fullWidth, 10) -- Push a small invisible block to create spacing

      -- -------------------------------- Leve List ------------------------------- --
      -- ANCHOR Leve List
      if LEDE.GUI.tabSelected == 1 then

        aa.PushStyle(aa.Layouts.transparentButton)
        GUI:Button('Filters:')
        aa.PopStyle(aa.Layouts.transparentButton)
        GUI:SameLine()
        if aa.TextureToggle(LEDE.GUI.leveFilter == 'CRP', LEDE.GUI.icons.CRP, 20, 20) then
          if LEDE.GUI.leveFilter == 'CRP' then
            LEDE.GUI.leveFilter = ''
          else
            LEDE.GUI.leveFilter = 'CRP'
          end
        end 
        if GUI:IsItemHovered() then
          GUI:SetTooltip('Carpenter leves only.')
        end GUI:SameLine()
        if aa.TextureToggle(LEDE.GUI.leveFilter == 'BSM', LEDE.GUI.icons.BSM, 20, 20) then
          if LEDE.GUI.leveFilter == 'BSM' then
            LEDE.GUI.leveFilter = ''
          else
            LEDE.GUI.leveFilter = 'BSM'
          end
        end
        if GUI:IsItemHovered() then
          GUI:SetTooltip('Blacksmith leves only.')
        end GUI:SameLine()
        if aa.TextureToggle(LEDE.GUI.leveFilter == 'ARM', LEDE.GUI.icons.ARM, 20, 20) then
          if LEDE.GUI.leveFilter == 'ARM' then
            LEDE.GUI.leveFilter = ''
          else
            LEDE.GUI.leveFilter = 'ARM'
          end
        end
        if GUI:IsItemHovered() then
          GUI:SetTooltip('Armorer leves only.')
        end GUI:SameLine()
        if aa.TextureToggle(LEDE.GUI.leveFilter == 'GSM', LEDE.GUI.icons.GSM, 20, 20) then
          if LEDE.GUI.leveFilter == 'GSM' then
            LEDE.GUI.leveFilter = ''
          else
            LEDE.GUI.leveFilter = 'GSM'
          end
        end
        if GUI:IsItemHovered() then
          GUI:SetTooltip('Goldsmith leves only.')
        end GUI:SameLine()
        if aa.TextureToggle(LEDE.GUI.leveFilter == 'LTW', LEDE.GUI.icons.LTW, 20, 20) then
          if LEDE.GUI.leveFilter == 'LTW' then
            LEDE.GUI.leveFilter = ''
          else
            LEDE.GUI.leveFilter = 'LTW'
          end
        end
        if GUI:IsItemHovered() then
          GUI:SetTooltip('Leatherworker leves only.')
        end GUI:SameLine()
        if aa.TextureToggle(LEDE.GUI.leveFilter == 'WVR', LEDE.GUI.icons.WVR, 20, 20) then
          if LEDE.GUI.leveFilter == 'WVR' then
            LEDE.GUI.leveFilter = ''
          else
            LEDE.GUI.leveFilter = 'WVR'
          end
        end
        if GUI:IsItemHovered() then
          GUI:SetTooltip('Weaver leves only.')
        end GUI:SameLine()
        if aa.TextureToggle(LEDE.GUI.leveFilter == 'ALC', LEDE.GUI.icons.ALC, 20, 20) then
          if LEDE.GUI.leveFilter == 'ALC' then
            LEDE.GUI.leveFilter = ''
          else
            LEDE.GUI.leveFilter = 'ALC'
          end
        end
        if GUI:IsItemHovered() then
          GUI:SetTooltip('Alchemist leves only.')
        end GUI:SameLine()
        if aa.TextureToggle(LEDE.GUI.leveFilter == 'CUL', LEDE.GUI.icons.CUL, 20, 20) then
          if LEDE.GUI.leveFilter == 'CUL' then
            LEDE.GUI.leveFilter = ''
          else
            LEDE.GUI.leveFilter = 'CUL'
          end
        end
        if GUI:IsItemHovered() then
          GUI:SetTooltip('Culinarian leves only.')
        end GUI:SameLine()
        if aa.TextureToggle(LEDE.GUI.leveFilter == 'FSH', LEDE.GUI.icons.FSH, 20, 20) then
          if LEDE.GUI.leveFilter == 'FSH' then
            LEDE.GUI.leveFilter = ''
          else
            LEDE.GUI.leveFilter = 'FSH'
          end
        end
        if GUI:IsItemHovered() then
          GUI:SetTooltip('Fisher leves only.')
        end GUI:SameLine()
        if aa.TextureToggle(LEDE.GUI.favFilter, LEDE.GUI.icons.star, 20, 20) then
          LEDE.GUI.favFilter = not LEDE.GUI.favFilter
        end
        if GUI:IsItemHovered() then
          GUI:BeginTooltip()
          GUI:Text('Favorite leves only.')
          GUI:Text('Can be combined with other filters')
          GUI:TextColored(0.1,0.9,0.1,1,'To set a levequest as favorite,\nright click it on the list.')
          GUI:EndTooltip()
        end 

        GUI:PushItemWidth(-300)
        aa.PushStyle(aa.Layouts.transparentButton)
        GUI:Button('Search:')
        aa.PopStyle(aa.Layouts.transparentButton)
        GUI:SameLine()
        LEDE.GUI.search = GUI:InputText('##LDLeveSearch', LEDE.GUI.search)
        GUI:PopItemWidth()
        GUI:SameLine()

        if (GUI:Button('Add to List##LDAddToList', 120, 19)) then
          if table.valid(LEDE.DB.AllLeves[LEDE.GUI.LeveSelect]) then
            LEDE.addNewLevequestToList(LEDE.DB.AllLeves[LEDE.GUI.LeveSelect])
            save = true
          end
        end

        GUI:BeginChild('##LDLeveList', -300, 200, true, GUI.WindowFlags_ForceVerticalScrollbar)
        local LeveList = {}
        local SortedLeveList = {}

        if not string.empty(LEDE.GUI.leveFilter) then
          LeveList = LEDE.DB.leves[LEDE.GUI.leveFilter]
        else
          LeveList = LEDE.DB.AllLeves
        end

        for key in pairs(LeveList) do table.insert(SortedLeveList, key) end
        table.sort(SortedLeveList, function(a, b) 
          if LeveList[a].levelRequired == LeveList[b].levelRequired then
            return LeveList[a].id < LeveList[b].id
          end
          return LeveList[a].levelRequired < LeveList[b].levelRequired
        end)

        for index, key in ipairs(SortedLeveList) do
          local entry = LeveList[key]
          local filtered = true
          if not string.empty(LEDE.GUI.search) then
            if not (LEDE.settings.useItemName and entry.item.name or entry.name):upper():find(
              LEDE.GUI.search:upper(), 1, true
            ) then
              filtered = false
            end
          end
          if LEDE.GUI.favFilter == true then
            if not LEDE.settings.favorites[key] then
              filtered = false
            end
          end
          if filtered then
            if GUI:Selectable( '##LEDESelectable' .. index, LEDE.GUI.LeveSelect == key ) then
              LEDE.GUI.LeveSelect = key
            end
            if GUI:IsItemClicked(1) then
              if LEDE.settings.favorites[key] then
                LEDE.settings.favorites[key] = nil
              else
                LEDE.settings.favorites[key] = true
              end
            end
            GUI:SameLine() GUI:Text((LEDE.settings.useItemName and entry.item.name or entry.name) .. (entry.triple and ' (!!!)' or ''))
            GUI:SameLine() GUI:Dummy(
              320 - GUI:CalcTextSize((LEDE.settings.useItemName and entry.item.name or entry.name) .. (entry.triple and ' (!!!)' or '')),
              13)
            if LEDE.settings.favorites[key] then
              GUI:SameLine() GUI:Image(LEDE.GUI.icons.star, 13, 13)
            end
          end
        end
        GUI:EndChild()
        GUI:SameLine()
        GUI:BeginChild('##LDLeveInfo', 0, 200, true)
        if LEDE.DB.AllLeves[LEDE.GUI.LeveSelect] then
          local leveInfo = LEDE.DB.AllLeves[LEDE.GUI.LeveSelect]
          GUI:Text(leveInfo.name)
          GUI:Text('Job: '.. leveInfo.job)
          GUI:Text('Level: ' .. leveInfo.levelRequired)
          GUI:Text('Item Name: ' .. leveInfo.item.name)
          if leveInfo.triple then
            GUI:TextColored(0.1, 0.9, 0.1, 1, 'Triple Delivery!')
          end
        end
        GUI:EndChild()

        GUI:Dummy(400, 10)
        GUI:Separator()

        local quantSave
        for index, leve in pairs(LEDE.state.leveList) do
          aa.PushStyle(aa.Layouts.transparentButton)
          GUI:Button(
            string.format(
              '%-44s', (LEDE.settings.useItemName and leve.item.name or leve.name) .. (leve.triple and ' (3)' or '')
            )
          )
          aa.PopStyle(aa.Layouts.transparentButton)
          GUI:SameLine()
          GUI:PushItemWidth(80)
          -- LuaFormatter off
          leve.count, quantSave = GUI:InputInt(
            '##LEDEQuantity' .. tostring(index), leve.count, 1, 1,
            GUI.InputTextFlags_CharsNoBlank + GUI.InputTextFlags_AlwaysInsertMode
          )
          -- LuaFormatter on
          if (quantSave) then
            leve.targetAmmount = leve.item.quantity * leve.count * (leve.triple and 3 or 1)
          end
          save = save or quantSave
          GUI:PopItemWidth()
          GUI:SameLine()
          if (GUI:Button('Remove##LDRemove' .. tostring(index), 67, 19)) then
            table.remove(LEDE.state.leveList, index)
            if LEDE.state.currentLeve == index then
              LEDE.state.currentLeve = nil
            end
            save = true
          end
          GUI:Separator()
        end
        GUI:Spacing()
      end

      -- -------------------------------- Settings -------------------------------- --
      -- ANCHOR Settings
      if LEDE.GUI.tabSelected == 2 then
        -- LuaFormatter off
        -- Delay configuration
        GUI:PushItemWidth(100)
        LEDE.settings.delayRate, save = GUI:InputInt(
          string.format('%s', 'Action Delay (milliseconds)') .. '##LEDEDelay',
          LEDE.settings.delayRate, 1, 10, GUI.InputTextFlags_CharsDecimal
        )
        if GUI:IsItemHovered()then
          GUI:SetTooltip('Delay when accepting and delivering the levequests.')
        end

        LEDE.settings.avoidFlying, save = GUI:Checkbox(
          string.format('%s', 'Avoid flying when running deliveries. Recommended on most situations') ..'##LEDEAvoidFlying',
          LEDE.settings.avoidFlying
        )

        -- Leve naming configuration
        LEDE.settings.useItemName, save = GUI:Checkbox(
          string.format('%s', 'Show item name instead of levequest name') ..'##LEDEUseItemName',
          LEDE.settings.useItemName
        )

        LEDE.settings.showDebugPrint, save = GUI:Checkbox(
          string.format('%s', 'Activate debug logging') .. '##LEDEDebugLog',
          LEDE.settings.showDebugPrint
        )
        GUI:PopItemWidth()
        LEDE.danger.forceEnable, _ =GUI:Checkbox(
          string.format('%s', 'Enable force reset')..'##LEDEForceResetCheckbox', LEDE.danger.forceEnable
        )
        if (LEDE.danger.forceEnable) then
          if (GUI:Button('Forcefully reset all settings##LEDEForceResetButton')) then
            LEDE.danger.forceEnable = false
            LEDE.state = {
              currentLeve = nil,
              leveList = {},
            }
            LEDE.settings = {
              throttleRate = 1000,
              useItemName = false,
              showDebugPrint = false
            } 
            aa.SaveSettings('LEDE', table.merge(LEDE.state, LEDE.settings))
          end
        end
        -- LuaFormatter on
      end

      -- ----------------------------- Database Tools ----------------------------- --
      -- ANCHOR DB Tools

      -- if LEDE.GUI.tabSelected == 3 then
      --   GUI:BeginChild('##LDDBTools', -230, 200, true, GUI.WindowFlags_ForceVerticalScrollbar)
      --   for levemeteID, levemete in pairs(LEDE.DB.levemetes) do
      --     local mapMissing = (levemete.mapid == 0)
      --     local posMissing = (levemete.pos.x == 0 and levemete.pos.y == 0 and levemete.pos.z == 0)
      --     if mapMissing or posMissing then
      --       GUI:Text(string.format('%-28s', 'Levemete --- ' .. levemete.name))
      --       GUI:SameLine()
      --       GUI:TextColored(0.9, 0.9, 0.1, 1, ' (' .. levemeteID .. ')')
      --       GUI:SameLine()
      --       GUI:TextColored(0.9, 0.1, 0.1, 1, (mapMissing and '|map|' or ''))
      --       GUI:SameLine()
      --       GUI:TextColored(0.9, 0.1, 0.1, 1, (posMissing and '|position|' or ''))
      --     end
      --   end
      --   for targetID, target in pairs(LEDE.DB.targets) do
      --     local mapMissing = (target.mapid == 0)
      --     local posMissing = (target.pos.x == 0 and target.pos.y == 0 and target.pos.z == 0)
      --     if mapMissing or posMissing then
      --       GUI:Text(string.format('%-28s', 'Target --- ' .. target.name))
      --       GUI:SameLine()
      --       GUI:TextColored(0.9, 0.9, 0.1, 1, ' (' .. targetID .. ')')
      --       GUI:SameLine()
      --       GUI:TextColored(0.9, 0.1, 0.1, 1, (mapMissing and '|map|' or ''))
      --       GUI:SameLine()
      --       GUI:TextColored(0.9, 0.1, 0.1, 1, (posMissing and '|position|' or ''))
      --     end
      --   end
      --   for leveID, leve in pairs(LEDE.DB.AllLeves) do
      --     local lMissing = (leve.levemeteID == 0 or leve.levemeteName == '')
      --     local tMissing = (leve.targetID == 0 or leve.targetName == '')
      --     if lMissing or tMissing then
      --       GUI:Text(string.format('%-28s', 'Leve --- ' .. leve.name))
      --       GUI:SameLine()
      --       GUI:TextColored(0.9, 0.9, 0.1, 1, ' (' .. leveID .. ')')
      --       GUI:SameLine()
      --       GUI:TextColored(0.9, 0.1, 0.1, 1, (lMissing and '|levemete|' or ''))
      --       GUI:SameLine()
      --       GUI:TextColored(0.9, 0.1, 0.1, 1, (tMissing and '|target|' or ''))
      --     end
      --   end
      --   GUI:EndChild()
      --   GUI:SameLine()

      --   if (GUI:Button('Update Table##LEDEUpdateTable')) then
      --     if LEDE.DB.levemetes[Player:GetTarget().contentid] ~= nil then
      --       LEDE.DB.levemetes[Player:GetTarget().contentid].mapid = Player.localmapid
      --       LEDE.DB.levemetes[Player:GetTarget().contentid].pos.x = Player:GetTarget().pos.x
      --       LEDE.DB.levemetes[Player:GetTarget().contentid].pos.y = Player:GetTarget().pos.y
      --       LEDE.DB.levemetes[Player:GetTarget().contentid].pos.z = Player:GetTarget().pos.z
      --     end
      --     if LEDE.DB.targets[Player:GetTarget().contentid] ~= nil then
      --       LEDE.DB.targets[Player:GetTarget().contentid].mapid = Player.localmapid
      --       LEDE.DB.targets[Player:GetTarget().contentid].pos.x = Player:GetTarget().pos.x
      --       LEDE.DB.targets[Player:GetTarget().contentid].pos.y = Player:GetTarget().pos.y
      --       LEDE.DB.targets[Player:GetTarget().contentid].pos.z = Player:GetTarget().pos.z
      --     end
      --     if IsControlOpen('GuildLeve') then
      --       for _, leveId in pairs(GetControl('GuildLeve'):GetData().quests) do
      --         LEDE.DB.AllLeves[leveId].levemeteID = Player:GetTarget().contentid
      --         LEDE.DB.AllLeves[leveId].levemeteName = Player:GetTarget().name
      --       end
      --     end
      --     FileSave(GetLuaModsPath() .. [[AALeveDelivery/TempDB.lua]], LEDE.DB)
      --   end
      -- end

      GUI:Dummy(fullWidth, 10)
      aa.DrawStartStopButton('Leve Delivery', 'LEDE', fullWidth, 30)

    end
    GUI:End()
  end

  -- Save settings if any stored parameter was changed
  if (save) then
    aa.SaveSettings('LEDE', table.merge(LEDE.settings, LEDE.state))
  end

end

--- Draw function for the main task window
-- @function Draw
-- @string event The event that triggered this functions
-- @number ticks Current time count
function LEDE.Draw(event, ticks)

  local fullWidth = GUI:GetContentRegionAvail()
  if (GUI:Button('Open Leve Manager', fullWidth, 30)) then
    LEDE.Open()
  end

  -- GUI:Text(LEDE.state.currentLeve)
  -- GUI:Text(tostring(LEDE.state.leveList))
  -- GUI:Text(tostring(LEDE.state.leveList[LEDE.state.currentLeve]))
  if LEDE.state.currentLeve ~= nil then
    GUI:Text(tostring(LEDE.GetGiver().name .. " || " .. LEDE.GetGiver().contentid))
    GUI:Text(tostring(LEDE.GetTaker().name .. " || " .. LEDE.GetTaker().contentid))
    GUI:Text("Quest: " .. tostring(LEDE.HasQuest()))
    GUI:Text("Item: " .. tostring(LEDE.HasItem()))
  end
  GUI:Spacing()
  for _, leve in pairs(LEDE.state.leveList) do
    if leve.hasItem then
      GUI:TextColored(0.2, 1, 0.2, 1, '|' .. leve.currentAmmount .. ' / ' .. leve.targetAmmount .. '|')
    elseif leve.delivering then
      GUI:TextColored(1, 1, 0.2, 1, '|' .. leve.currentAmmount .. ' / ' .. leve.targetAmmount .. '|')
    else
      GUI:TextColored(1, 0.2, 0.2, 1, '|' .. leve.currentAmmount .. ' / ' .. leve.targetAmmount .. '|')
    end
    GUI:SameLine()
    GUI:Text(leve.item.name)
  end

end

-- -------------------------------------------------------------------------- --
-- ANCHOR                             Logic                                   --
-- -------------------------------------------------------------------------- --

function LEDE.addNewLevequestToList(levequest)
  local presentInCurrentList = false
  for _, entry in pairs(LEDE.state.leveList) do
    if (entry.name == levequest.name) then
      presentInCurrentList = true
    end
  end
  if not presentInCurrentList then
    local newleve = table.deepcopy(levequest)
    newleve.count = 1
    newleve.delivered = 0
    newleve.delivering = false
    newleve.hasItem = false
    newleve.hasQuest = false
    newleve.currentAmmount = 0
    newleve.targetAmmount = newleve.item.quantity * (newleve.triple and 3 or 1)
    table.insert(LEDE.state.leveList, newleve)
  end
end

function LEDE.removeLevequestFromListById(id)
  for i, entry in pairs(LEDE.state.leveList) do
    if (entry.id == id) then
      table.remove(LEDE.state.leveList, i)
    end
  end
end

function LEDE.removeLevequestFromListByName(name)
  for i, entry in pairs(LEDE.state.leveList) do
    if (entry.name == name) then
      table.remove(LEDE.state.leveList, i)
    end
  end
end

function LEDE.RemoveLevequestFromListByIndex(index)
  table.remove(LEDE.state.leveList, index)
end

--- Update the item counter for the specified levequest
-- @function UpdateLeveCounters
function LEDE.UpdateLeveCounters(leve)
  if LEDE.settings.showDebugPrint then
    d('Updating leve counters for ' .. leve.name)
  end
  if leve ~= nil then
    if leve.count == 0 then
      local index = aa.FindIndex(LEDE.state.leveList, 
        function(leveIndex) return LEDE.state.leveList[leveIndex].id == leve.id end)
      LEDE.state.leveList[index] = nil
      LEDE.state.currentLeve = nil
    else
      leve.currentAmmount = ItemCount(leve.item.id, { 0, 1, 2, 3 }, true)
      leve.targetAmmount = leve.item.quantity * leve.count * (leve.triple and 3 or 1)
      if leve.currentAmmount >= leve.targetAmmount then
        leve.hasItem = true
      else
        leve.hasItem = false
      end
    end
  end
end

--- Update the quest status for the specified levequest
-- @function UpdateLevetStatus
function LEDE.UpdateLeveStatus(leve)
  if (table.valid(Quest:GetLeveList()[leve.id])) then
    leve.hasQuest = true
  else
    leve.hasQuest = false
  end
end

--- Returns true if the quest for the currently selected leve was taken
-- @function HasQuest
function LEDE.HasQuest()
  if LEDE.state.currentLeve ~= nil then
    return LEDE.state.leveList[LEDE.state.currentLeve].hasQuest
  end
  return false
end

--- Returns true if you have the necessary items to make a complete delivery
-- @function HasItem
function LEDE.HasItem()
  if LEDE.state.currentLeve ~= nil then
    return LEDE.state.leveList[LEDE.state.currentLeve].hasItem
  end
end

--- Returns levequest levemete npc reference
-- @function GetGiver
function LEDE.GetGiver()
  if LEDE.state.currentLeve ~= nil then
    return LEDE.DB.levemetes[LEDE.state.leveList[LEDE.state.currentLeve].levemeteID]
  end
  return false
end

--- Returns levequest target npc reference
-- @function GetTaker
function LEDE.GetTaker()
  if LEDE.state.currentLeve ~= nil then
    return LEDE.DB.targets[LEDE.state.leveList[LEDE.state.currentLeve].targetID]
  end
  return false
end

--- Returns which npc we should go to now
-- @function GetRelevantNPC
function LEDE.GetRelevantNPC()
  if LEDE.HasQuest() and LEDE.HasItem() then
    return LEDE.GetTaker()
  elseif LEDE.HasItem() and not LEDE.HasQuest() then
    return LEDE.GetGiver()
  end
  return nil
end

-- -------------------------------------------------------------------------- --
-- ANCHOR                       Cause and Effects                             --
-- -------------------------------------------------------------------------- --

--- Cause and Effects
-- @section Cause and Effect

LEDE.CnE = {}

-- -------------------------------- Overwatch ------------------------------- --

--- Skip all talk dialogs if they are open
LEDE.CnE.skipTalk = {
  c = inheritsFrom(ml_cause),
  e = inheritsFrom(ml_effect),
 }

function LEDE.CnE.skipTalk.c.evaluate()
  if IsControlOpen('Talk') then
    return true
  end
  return false
end

function LEDE.CnE.skipTalk.e.execute()
  if LEDE.settings.showDebugPrint then
    d('Executing skipTalk')
  end
  UseControlAction('Talk', 'Click')
end

--- Handle all the SelectString controls
LEDE.CnE.skipSelectString = {
  c = inheritsFrom(ml_cause),
  e = inheritsFrom(ml_effect),
 }

function LEDE.CnE.skipSelectString.c.evaluate()
  if IsControlOpen('SelectString') then
    return true
  end
  return false
end

function LEDE.CnE.skipSelectString.e.execute()
  if LEDE.settings.showDebugPrint then
    d('Executing skipSelectString')
  end
  -- If taker is the target
  if Player:GetTarget().contentid == LEDE.GetTaker().contentid then
    if LEDE.settings.showDebugPrint then
      d('Taker')
    end
    UseControlAction('SelectString', 'SelectIndex', 0)

  -- if giver is the target
  elseif Player:GetTarget().contentid == LEDE.GetGiver().contentid then 
    if LEDE.settings.showDebugPrint then
      d('Giver')
    end
    if LEDE.state.leveList[LEDE.state.currentLeve].hasQuest then
      if table.size(GetControl('SelectString'):GetData()) == 8 then
        UseControlAction('SelectString', 'SelectIndex', 2)
      elseif table.size(GetControl('SelectString'):GetData()) == 4 then
        UseControlAction('SelectString', 'SelectIndex', 3)
      elseif table.size(GetControl('SelectString'):GetData()) == 5 then
        UseControlAction('SelectString', 'SelectIndex', 4)
      end
    else
      UseControlAction('SelectString', 'SelectIndex', 0)
    end
  end
end

--- Handle the Leve selection on delivery
LEDE.CnE.skipSelectIconString = {
  c = inheritsFrom(ml_cause),
  e = inheritsFrom(ml_effect),
 }

function LEDE.CnE.skipSelectIconString.c:evaluate()
  if IsControlOpen('SelectIconString') then
    return true
  end
  return false
end

function LEDE.CnE.skipSelectIconString.e:execute()
  if LEDE.settings.showDebugPrint then
    d('Executing skipSelectIconString')
  end
  if LEDE.state.leveList[LEDE.state.currentLeve].hasItem then
    UseControlAction(
      'SelectIconString', 'SelectIndex',
        table.find(GetControl('SelectIconString'):GetData(), LEDE.state.leveList[LEDE.state.currentLeve].name)
    )
  end
end

--- Handle all Yes/No controls
LEDE.CnE.skipSelectYesno = {
  c = inheritsFrom(ml_cause),
  e = inheritsFrom(ml_effect),
 }

function LEDE.CnE.skipSelectYesno.c.evaluate()
  if IsControlOpen('SelectYesno') then
    return true
  end
  return false
end

function LEDE.CnE.skipSelectYesno.e.execute()
  if LEDE.settings.showDebugPrint then
    d('Executing skipSelectYesNo')
  end
  UseControlAction('SelectYesno', 'Yes')
end

--- Handle the closure of the Levequest window
LEDE.CnE.closeGuildLeveList = {
  c = inheritsFrom(ml_cause),
  e = inheritsFrom(ml_effect),
 }

function LEDE.CnE.closeGuildLeveList.c.evaluate()
  if IsControlOpen('GuildLeve') and table.valid(Quest:GetLeveList()[LEDE.state.leveList[LEDE.state.currentLeve].id]) then
    return true
  end
  return false
end

function LEDE.CnE.closeGuildLeveList.e.execute()
  if LEDE.settings.showDebugPrint then
    d('Executing closeGuildLeveList')
  end
  GetControl('GuildLeve'):Close()
end

--- Activate the handover prompt slots
LEDE.CnE.handOverLeveItem = {
  c = inheritsFrom(ml_cause),
  e = inheritsFrom(ml_effect),
 }

function LEDE.CnE.handOverLeveItem.c.evaluate()
  if IsControlOpen('Request') then
    return true
  end
  return false
end

function LEDE.CnE.handOverLeveItem.e.execute()
  if LEDE.settings.showDebugPrint then
    d('Executing handOverLeveItem')
  end
  ml_global_information.Await(LEDE.settings.delayRate)
  local currentLeveInfo = LEDE.state.leveList[LEDE.state.currentLeve]
  if (GetItem(currentLeveInfo.item.id + 1000000)) then
    GetItem(currentLeveInfo.item.id + 1000000):HandOver()
  elseif (GetItem(currentLeveInfo.item.id)) then
    GetItem(currentLeveInfo.item.id):HandOver()
  end
  currentLeveInfo.delivering = true
  UseControlAction('Request', 'HandOver', 0)
end

-- --------------------------------- Process -------------------------------- --

--- Navigate to the relevant npc map if the player is not in it
LEDE.CnE.moveToNPCMap = {
  c = inheritsFrom(ml_cause),
  e = inheritsFrom(ml_effect),
}

function LEDE.CnE.moveToNPCMap.c:evaluate()
  if LEDE.GetRelevantNPC() then
    if Player.localmapid ~= LEDE.GetRelevantNPC().mapid then
      return true
    end
  end
  return false
end

function LEDE.CnE.moveToNPCMap.e:execute()
  local task = ffxiv_task_movetomap.Create()
  task.destMapID = LEDE.GetRelevantNPC().mapid
  ml_task_hub:CurrentTask():AddSubTask(task)
end

--- Move to the aproximate position of the relevant npc
LEDE.CnE.moveToNPCPos = {
  c = inheritsFrom(ml_cause),
  e = inheritsFrom(ml_effect),
 }

function LEDE.CnE.moveToNPCPos.c:evaluate()
  if LEDE.GetRelevantNPC() then
    if Player.localmapid == LEDE.GetRelevantNPC().mapid then
      if table.valid(EntityList('maxdistance=7,contentid=' .. LEDE.GetRelevantNPC().contentid)) then
        return false
      end
      return true
    end
  end
  return false
end

function LEDE.CnE.moveToNPCPos.e:execute()
  local task = ffxiv_task_movetointeract.Create()
  task.cubefilters = (LEDE.settings.avoidFlying == true and 1 or LEDE.settings.avoidFlying == false and 0)
  task.contentid = LEDE.GetRelevantNPC().contentid
  task.pos = LEDE.GetRelevantNPC().pos
  task.range = math.random(1, 4)
  ml_task_hub:CurrentTask():AddSubTask(task)
end

-- The npc is alreay nearby, just talk to him
LEDE.CnE.interactWithNPCNearby = {
  c = inheritsFrom(ml_cause),
  e = inheritsFrom(ml_effect),
 }

function LEDE.CnE.interactWithNPCNearby.c:evaluate()
  if LEDE.GetRelevantNPC() then
    if table.valid(EntityList('maxdistance=7,contentid=' .. LEDE.GetRelevantNPC().contentid)) then
      if not Busy() then
        return true
      end
    end
  end
  return false
end

function LEDE.CnE.interactWithNPCNearby.e:execute()
  local entity = next(EntityList('maxdistance=7,contentid=' .. LEDE.GetRelevantNPC().contentid))
  if Player:GetTarget() then
    if Player:GetTarget().contentid == LEDE.GetRelevantNPC().contentid then
      Player:Interact(Player:GetTarget().id)
      return
    end
  end
  Player:SetTarget(entity)
end

--- Handles levequest acceptance
LEDE.CnE.acceptLeveQuest = {
  c = inheritsFrom(ml_cause),
  e = inheritsFrom(ml_effect),
 }

function LEDE.CnE.acceptLeveQuest.c.evaluate()
  if IsControlOpen('GuildLeve') and not table.valid(Quest:GetLeveList()[LEDE.state.leveList[LEDE.state.currentLeve].id]) then
    return true
  end
  return false
end

function LEDE.CnE.acceptLeveQuest.e.execute()
  LEDE.UpdateLeveCounters(LEDE.state.leveList[LEDE.state.currentLeve])
  ml_global_information.Await(LEDE.settings.delayRate)
  UseControlAction('GuildLeve', 'AcceptQuest', LEDE.state.leveList[LEDE.state.currentLeve].id)    
end

--- Confirm the levequest completion
LEDE.CnE.completeLeveQuest = {
  c = inheritsFrom(ml_cause),
  e = inheritsFrom(ml_effect),
 }

function LEDE.CnE.completeLeveQuest.c.evaluate()
  if IsControlOpen('JournalResult') then
    return true
  end
  return false
end

function LEDE.CnE.completeLeveQuest.e.execute()
  if LEDE.settings.showDebugPrint then
    d('Executing handOverLeveItem')
  end
  local currentLeveInfo = LEDE.state.leveList[LEDE.state.currentLeve]
  if LEDE.settings.showDebugPrint then
    d(tostring(LEDE.state.leveList[LEDE.state.currentLeve]))
  end
  UseControlAction('JournalResult', 'Complete')
  if (currentLeveInfo.triple) then
    currentLeveInfo.delivered = currentLeveInfo.delivered + 1
    if currentLeveInfo.delivered == 3 then
      currentLeveInfo.count = currentLeveInfo.count - 1
      currentLeveInfo.delivered = 0
      currentLeveInfo.delivering = false
      LEDE.UpdateLeveCounters(currentLeveInfo)
      aa.SaveSettings('LEDE', table.merge(LEDE.settings, LEDEc.state))
    end
  else
    currentLeveInfo.count = currentLeveInfo.count - 1
    LEDE.UpdateLeveCounters(currentLeveInfo)
    aa.SaveSettings('LEDE', table.merge(LEDE.settings, LEDE.state))
  end
end

--- Select the next leve to proccess
-- @table selectNextLeve
-- @field c Cause definition
-- @field e Evaluation definition
LEDE.CnE.selectNextLeve = {
  c = inheritsFrom(ml_cause),
  e = inheritsFrom(ml_effect),
 }

function LEDE.CnE.selectNextLeve.c.evaluate()
  if LEDE.settings.showDebugPrint then
    d('Evaluating selectNexLeve')
  end
  local leveIndex = LEDE.state.currentLeve

  if leveIndex == nil then
    if table.valid(LEDE.state.leveList) then
      if LEDE.settings.showDebugPrint then
        d('Looking for a valid leve')
      end
      leveIndex = aa.FindIndex(
                    LEDE.state.leveList, function(index, leve) -- Find first leve that has enough items to deliver
                      return leve.hasItem
                    end
                  )
      if leveIndex ~= nil then
        if LEDE.settings.showDebugPrint then
          d('Found a leve: ' .. LEDE.state.leveList[leveIndex].name)
        end
        LEDE.state.currentLeve = leveIndex
      else
        if LEDE.settings.showDebugPrint then
          d('Could not find a valid leve')
        end
        return true
      end
    end
  else
    if LEDE.state.leveList[leveIndex] then
      LEDE.UpdateLeveStatus(LEDE.state.leveList[leveIndex])
      if LEDE.state.leveList[leveIndex].hasItem then
        if LEDE.settings.showDebugPrint then
          d('Still have a valid leve')
        end
        return false
      end
    end
  end
  if leveIndex == nil then

  end
  if aa.Every(
    LEDE.state.leveList, function(leve)
      if (leve.count == 0 or leve.hasItem == false) and leve.delivering == false then
        return true
      end
    end
  ) then
    if IsControlOpen('Talk') or IsControlOpen('SelectString') or IsControlOpen('SelectYesNo') or
      IsControlOpen('Guildleve') or IsControlOpen('JournalResult') then
      return false
    end
    return true
  end
  return false
end

function LEDE.CnE.selectNextLeve.e.execute()
  if LEDE.settings.showDebugPrint then
    d('Executing selectNextLeve')
  end
  LEDE.state.currentLeve = nil
  aa.SaveSettings('LEDE', table.merge(LEDE.settings, LEDE.state))
  ml_global_information.ToggleRun()
end

-- @section end

-- -------------------------------------------------------------------------- --
-- ANCHOR                           Database                                  --
-- -------------------------------------------------------------------------- --

LEDE.DB = {}

-- -------------------------------------------------------------------------- --
--  ANCHOR                   Registration and Return                          --
-- -------------------------------------------------------------------------- --

RegisterEventHandler('Module.Initalize', LEDE.ModuleInit, '[AnonAddons] LEDE Init')
RegisterEventHandler('Gameloop.Draw', LEDE.ModuleDraw, '[AnonAddons] LEDE Draw')

return LEDE